# Tabletop Task

# Log of Bugs and Fixes

## Bugs

- The parameter passed to the 'delete' and 'get' functions were not used.
- The function 'delete' doesn't actually delete anything, but just reduces the count by 1 and returns the first value when it should literally delete. Additionally, the 'count' property must only be deducted if the specific registration was actually removed from the 'garage' variable.
- The count started at 1, whereas there were 3 car elements in the "cars" array, but even the third one was useless (assuming the 'count' property refers to the number of cars in the variable).
- Instead of actually adding the new car registration into the "cars" property, it overwrites the last property with the new one.
- The 'get' function doesn't return the desired registration number but instead returns a count number.

## Fixes

- Added a check to ensure that no undefined or empty object is added to the 'garage' variable. (not just registration number)
- Changed the name of the 'value' parameter to 'newCar' to better understand what is passed into the 'add' function.
- Fixed a typo in the word 'rag'. Now only the values for 'reg' are checked specifically.
- Implemented validation for registration numbers based on DVLA official rules (i.e., 2 letters, 2 numbers, space, 3 random letters), and checked if the registration matches the one stored here, regardless of spaces (i.e., at least 7 symbols in length).


## Features

- Utilized the 'axios' library for promise-based API calls due to its simplicity, flexibility in terms of browsers, configurations, and error handling. The choice was also influenced by its presence in the DVLA API documentation.

- Modified the private 'garage' variable in garage.js to store only essential car information, including the registration number, make, and year.

- Due to the unavailability of additional information from the API calls (such as the model and image of the car), those details were excluded from the implementation, and the focus was placed on the retrieved information.

- To address the 'cross-origin' issue encountered during the API calls, a proxy was utilized to authorize requests. The proxy service used for this purpose is [https://cors-anywhere.herokuapp.com/](https://cors-anywhere.herokuapp.com/).