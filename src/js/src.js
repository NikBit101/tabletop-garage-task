import axios from 'axios';

window.addEventListener("garage-loaded", start, false);

function callAPI(regNum) {
  console.log(regNum);
  const config = {
    method: 'post',
    url: 'https://driver-vehicle-licensing.api.gov.uk/vehicle-enquiry/v1/vehicles',
    headers: {
      'x-api-key': '',
      'Content-Type': 'application/json',
    },
    data: JSON.stringify(regNum),
  };
  
  axios(config)
  .then(function(response) {
    const data = {
      reg: response.data.registrationNumber,
      make: response.data.make,
      year: response.data.yearOfManufacture
    };
    console.log(data);
    const garageEvent = new CustomEvent("add-to-garage", { detail: data });
    window.dispatchEvent(garageEvent);
  })
  .catch(function(error) {
    if(error.response.status === 404) {
      const data = {
        reg: regNum.registrationNumber,
        make: 'No information',
        year: 'No information'
      };
      console.log(data);
      const garageEvent = new CustomEvent("add-to-garage", { detail: data });
      window.dispatchEvent(garageEvent);
    }
    console.error(error);
  });
}

function start() {
  // testing an API call
  callAPI({ "registrationNumber": "AA19AAA" });
}

window.addEventListener("add-car", function(event) {
  console.log(event.detail);
  callAPI(event.detail);
});