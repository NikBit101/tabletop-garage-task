const garage = {
    "count": 2,
    "cars":[
        {
            'reg':'AA19 BBB',
            'make':'MERCEDES-BENZ',
            'year':2019
        },
        {
            'reg':'AA19EEE',
            'make':'No information',
            'year':'No information'
        },
    ]
};

export const Garage = {   
    add(newCar){
        const wMessage = document.querySelector('#warningMSG');

        // empty or undefined object passed
        if (newCar.reg === undefined) {
            console.log('No input for registration number!');
            wMessage.textContent = 'Input a registration number!';
            return;
        }
        // invalid registration number is spotted
        const regPattern = /^[A-Z]{2}\d{2}(\s?[A-Z]{3})?$/;
        if (!regPattern.test(newCar.reg)) {
            console.log(`Invalid format of Registration number! - ${newCar.reg}`);
            wMessage.textContent = `Invalid format of Registration number! - ${newCar.reg}`;
            return;
        }
        // passed registration number already exists
        // ignore whitespaces
        for (let i = 0; i < garage.cars.length; i++) {
            if (garage.cars[i].reg.replace(/\s/g, '') === newCar.reg.replace(/\s/g, '')) {
                console.log(`Car with registration number ${newCar.reg} already exists in the garage.`);
                wMessage.textContent = `Car with registration number ${newCar.reg} already exists in the garage.`;
                return;
            }
        }
        // add & dispatch the event to make an API call for registration check
        const addEvent = new CustomEvent('add-car', { detail: { registrationNumber: newCar.reg.toUpperCase() } })
        window.dispatchEvent(addEvent);
    },
        delete(reg){
            const wMessage = document.querySelector('#warningMSG');
            wMessage.textContent = '';
            
            if (reg === undefined || reg === '') {
                wMessage.textContent = 'enter registration number!';
                return 'enter registration number!';
            }

            for (let i = 0; i < garage.cars.length; i++) {
                if (garage.cars[i].reg === reg) {
                    garage.cars.splice(i, 1); // Remove the car object from the array
                    garage.count--;
                    break; // Exit the loop since the car was found and removed
                }
            }
            wMessage.textContent = `Registration number ${reg} has been deleted!`;
            return `Registration number ${reg} has been deleted!`;
    },
        get(reg){
            const wMessage = document.querySelector('#warningMSG');
            wMessage.textContent = '';

            for (let i = 0; i < garage.cars.length; i++) {
                if (garage.cars[i] === undefined) { continue; } // reg number is not valid
                if (garage.cars[i].reg.replace(/\s/g, '') === reg.replace(/\s/g, '')) {
                    // reg input matches the one stored in 'garage', regardless of the spacing
                    document.querySelector('#reg').textContent = `Registration: ${garage.cars[i].reg}`;
                    document.querySelector('#make').textContent = `Make: ${garage.cars[i].make}`;
                    document.querySelector('#year').textContent = `Year: ${garage.cars[i].year}`;
                    wMessage.textContent = '';
                    return garage.cars[i];
                }
            }
            wMessage.textContent = `Sorry, no such registration number exists - ${reg}`;
            return `Sorry, no such registration number exists - ${reg}`; // if no match found, return error message
    }
}

window.addEventListener("add-to-garage", function(e) {
            // if response is successful, record the car details as object, and store into 'garage'
            garage.cars[garage.count] = {
                reg: e.detail.reg,
                make: e.detail.make,
                year: e.detail.year
            };
            garage.count += 1;
            console.log(garage);
} );
document.querySelector('#regAdd').addEventListener("click", function() {
    // clear all elements of text content
    document.querySelector('#reg').textContent = ``;
    document.querySelector('#make').textContent = ``;
    document.querySelector('#year').textContent = ``;

    const regValue = document.querySelector('#regNumber').value.toUpperCase();
    Garage.add( { reg: regValue } );
});
document.querySelector('#regDel').addEventListener("click", function() {
    document.querySelector('#reg').textContent = '';
    document.querySelector('#make').textContent = '';
    document.querySelector('#year').textContent = '';
    
    const regValue = document.querySelector('#regNumber').value.toUpperCase();
    Garage.delete(regValue);
});
document.querySelector('#regGet').addEventListener("click", function() {
    document.querySelector('#reg').textContent = ``;
    document.querySelector('#make').textContent = ``;
    document.querySelector('#year').textContent = ``;
    
    const regValue = document.querySelector('#regNumber').value.toUpperCase();
    Garage.get(regValue);
});




// log-of-bugs //
// the parameter passed to 'delete' and 'get' functions were not used.
/* The function 'delete' doesn't actually delete anything, but just reduces count by 1 and returns the first value, when it should literally delete
   Additionally, the 'count' property must only be deducted if the specific registration was actually removed from 'garage' variable.
*/
/* The count started at 1, whereas there were 3 car elements in "cars" array, but even the third one was useless
    (I'm assuming the 'count' property refers to amount of cars in the variable)
*/
// Instead of actually adding the new car registration into the "cars" property, it overwrites the last property into new
// The 'get' function doesn't do what it needs to do. Instead of returning the desired reg. number, it returns a count number.
// ---

// fixes //
// Added a check for making sure no undefined or empty object is added into 'garage' variable
// Changed the name of 'value' to 'newCar' to better understand what is passed into 'add' function
// I'm assuming that there was a typo of the word 'rag'. So I fixed it here and only checked the values for 'reg' specifically.
/* Accepted only the numbers based on DVLA official rules (i.e. 2 letters, 2 numbers, space, 3 random letters), and checked if the
   registration matches the one stored here, regardless of spaces (i.e. at least 7 symbols in length) */
// ---